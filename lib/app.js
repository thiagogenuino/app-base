var express = require('express')
  , app = module.exports = express()
  , server = require('http').Server(app)
  , io = require('socket.io')(server)
  , path = require('path')
  , fs = require('fs')
  , pkg = require('./../package.json')
  , config = require('./../config.json')
  , app_path = path.resolve(__dirname+'/../app/')
  , base_path = path.resolve(__dirname+'/../')
  , mongoose = require('mongoose')
  , modelExtend = require('mongoose-schema-extend')
  , connection = null
  , exec = require('child_process').exec
  , mongocmd = null
;

app.set('views', path.join(app_path, 'views'));
app.set('view engine', 'jade');
app.use('/', express["static"](path.join(base_path, 'public')));

if (config.database.host) {
  app.connection = mongoose.connect(config.database.host);
  start();
} else {
  fs.mkdir('./database', function(err) {
    console.log('mongo database started at port 27017 and path ./database')
    mongocmd = exec('mongod --dbpath="./database"', function(err, stdout, stderr) {
      
    });
    app.connection = mongoose.connect('mongodb://localhost/'+pkg.name, function() {
      start();
    });
    
    process.on('exit', function() {
      mongocmd.kill();
    });
  });
}

var start = function() {
  app.io = io;
  app.model = function(name) {
    return require(path.join(app_path, 'models', name+'.js'));
  };
  app.controller = function(name) {
    return require(path.join(app_path, 'controllers', name+'.js'));
  };

  app.filters = require( path.join(app_path, 'filters.js') );

  require(path.join(app_path, 'routes.js'));
  require(path.join(app_path, 'sockets.js'));

  server.listen(config.port, function() {
    var host, port;
    host = server.address().address;
    port = server.address().port;
    return console.log('app listening at %s', port);
  });
};