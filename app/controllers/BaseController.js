var main = require.main.exports;

module.exports = {
  actions: {
    home: function(req, res) {
      res.render('home', {
        io_port: req.protocol+'://'+req.headers.host
      });
    },
    load_views: function(req, res) {
      var call = req.params[0].substring(1);
      res.render(call);
    }
  },


};