var main = require.main.exports;
var bcrypt = require('bcryptjs');
var conn = main.connection;
var validator = require('validator');

// Validate the user status
var statusValidator = function(val) {
    return (
        (val === 1)
        || (val === 0)
        );
};

var roleValidator = function(val) {
    return (
        (val === 1)
        || (val === 2)
        );
};

var emailValidator = function(val, cb) {
    cb(validator.isEmail(val));
};

var emailExistsValidator = function(val, cb) {
    var query = {email: val};
    if (!this.isNew) {
        query['_id'] = {'$ne': this._id};
    }
    main.api('user')
        .findOne(query)
            .exec(function(err, result) {
                if (err)
                    cb(false);
                else {

                    if (result)
                        cb(false);
                    else
                        cb(true);

                }
            });
};

var Schema = main.model('base-schema').extend({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        validate: [
            {
                validator: emailValidator,
                msg: 'invalid Email'
            },
            {
                validator: emailExistsValidator,
                msg: 'Email already in use'
            }
        ]
    },
    password: {
        type: String
    },

    // Roles are
    // 1= admin
    // 2= editor
    role: {
        type: Number,
        default: 2,
        validate: [
            {
                validator: roleValidator,
                msg: 'Invalid role'
            }
        ]
    },

    // Status are
    // 1= active
    // 0= inactive
    status: {
        type: Number,
        default: 1,
        validate: [
            {
                validator: statusValidator,
                msg: 'Invalid status'
            }
        ]
    }

});

Schema.index({ user: 1 });

Schema.pre('save', function(next) {
    var obj = this;
    if (this.modifiedPaths().indexOf('password') !== -1) {
        bcrypt.hash(this.password, 10, function(err, hash) {
            if (err)
                throw err;

            obj.password = hash;
            next();
        });
    } else {
        delete this.password;
        next();
    }

});

module.exports = conn.model('user', Schema);