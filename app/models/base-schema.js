var main = require.main.exports;
var conn = main.connection;
var ObjectId = conn.Schema.Types.ObjectId;

var Schema = new conn.Schema({

    created_by: { type: ObjectId, ref: 'user' },
    updated_by: { type: ObjectId, ref: 'user' },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }

}, {
    toObject: {
      virtuals: true
    }
    ,toJSON: {
      virtuals: true
    }
});

Schema.pre('save', function(next) {
    var obj = this;
    
    obj.updated_at = Date.now();
    next();
});

module.exports = Schema;