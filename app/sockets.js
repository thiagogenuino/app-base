var io = require.main.exports.io;

io.on('connection', function (socket) {
  socket.emit('hello', 'hello client');
  
  socket.on('hello', function (data) {
    console.log(data);
  });
});