var app = require.main.exports;

var default_filter = app.filters.default;

app.get('/', default_filter, app.controller('BaseController').actions.home);
app.get(/(.*)/, default_filter, app.controller('BaseController').actions.load_views);