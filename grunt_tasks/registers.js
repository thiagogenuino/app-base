// Grunt registers
// ---------------------------------

module.exports = function(grunt) {
  // Stylus
  grunt.registerTask( 'styl', ['stylus'] );

  // Js
  grunt.registerTask('js', ['jshint', 'uglify']);

  // CSS
  grunt.registerTask('css', ['stylus', 'combine_mq', 'cssmin']); 

  // Build
  grunt.registerTask( 'build', ['imagemin', 'jshint', 'uglify', 'stylus', 'combine_mq', 'cssmin' ] );

  // Watch
  grunt.registerTask( 'w', ['browserSync', 'watch' ] );
};