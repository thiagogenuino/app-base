module.exports = {
  name: 'stylus',
  config: {
    dev: {
      options: { 
        compress: false,
        paths: [
          'node_modules/grunt-contrib-stylus/node_modules',
          'node_modules/jeet/stylus',
          'node_modules/nib/lib'
        ]
      },
      files: {
        '<%= build %>/css/style.css': '<%= src %>/stylus/style.styl'
      }
    } 
  }
};