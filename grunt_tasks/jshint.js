module.exports = {
  name: 'jshint',
  config: {
    ignore_warning: {
      options: {
        '-W033': true,
        '-W099': true,
      },
      src: ['<%= src %>/scripts/main.js', '<%= src %>/scripts/functions/*.js'],
    },
  },
}