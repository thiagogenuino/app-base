module.exports = {
  name: 'watch',
  config: {
    options: {
      interval: 200
    },
    stylus: {
      files: [
        '<%= src %>/stylus/**/*.styl'
      ],
      tasks: ['stylus', 'combine_mq', 'cssmin']
    },

    js: {
      files: ['<%= src %>/scripts/*.js', '<%= src %>/scripts/**/*.js'],
      tasks: ['uglify:dev']
    },

    img: {
      files: [
        '<%= src %>/images/**/*.{png,jpg,jpeg,gif,svg}'
      ],
      tasks: ['imagemin']
    },

    build: {
      files: [
        'Gruntfile.js'
      ],
      tasks: ['build']
    }
  }
}