module.exports = {
  name: 'imagemin',
  config: {
    img: {
      options: {
        progressive: true,
        interlaced: true,
      },
      files: [{
        expand: true,
        cwd: '<%= src %>/images/',
        src: [
          '**/*.{png,jpg,jpeg,gif}'
        ],
        dest: '<%= build %>/images'
      }],
    },
    svg: {
      files: [{
        expand: true,
        cwd: '<%= src %>/svg/',
        src: [
          '**/*.{svg}'
        ],
        dest: '<%= build %>/svg/'
      }],
    }
  }
}