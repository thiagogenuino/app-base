module.exports = {
  name: 'uglify',
  config: { 
    vendor: {
      files: {
        '<%= src %>/scripts/_vendor.js': [

          'bower_components/jquery/dist/jquery.js',                   // jQuery 
          'bower_components/picturefill/dist/picturefill.js',         // picturefill 
          // 'custom_components/onepagescroll.js',                       // onepage-scroll

        ]
      }
    },

   dev: {  
      options: {
        mangle: false, 
        compress : false,
        sourceMap: true,
        report: 'min'
      }, 
      files: { 
        '<%= build %>/scripts/app.min.js': [
          '<%= src %>/scripts/_vendor.js',
          '<%= src %>/scripts/functions/*',  
          '<%= src %>/scripts/main.js'
        ]
      }
    },

    build: { 
      files: { 
        '<%= build %>/scripts/app.min.js': [
          '<%= src %>/scripts/_vendor.js',
          '<%= src %>/scripts/functions/*',  
          '<%= src %>/scripts/main.js'
        ]
      }
    }
  }
}