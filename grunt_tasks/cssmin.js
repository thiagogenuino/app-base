module.exports = {
  name: 'cssmin',
  config: {
    options: {
      shorthandCompacting: false,
      roundingPrecision: -1
    },
    target: {
      files: { 
        '<%= build %>/css/style.css': '<%= build %>/css/style.css', 
      }
    }
  }
}