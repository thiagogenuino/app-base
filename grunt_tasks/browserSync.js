var config = require('./../config.json');
module.exports = {
  name: 'browserSync',
  config: {
    bsFiles: {
      src : [
        '<%= build %>/css/*.css',
        '<%= build %>/scripts/*.js',
        '<%= build %>/**/*.jpg',
        '<%= build %>/**/*.png',
        '<%= build %>/**/*.svg'
      ]
    },
    options: {
      watchTask: true,
      proxy : 'localhost:'+config.port,
      ghostMode: {
        clicks: true,
        scroll: true,
        links: true,
        forms: true
      }
    }
  }
}