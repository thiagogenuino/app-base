var fs = require('fs');

module.exports = function(grunt) {

  // Grunt Time
  // ---------------------------------
  require('time-grunt')(grunt);

   // Load all tasks
  // ---------------------------------
  require('jit-grunt')(grunt);

  var grunt_cfg = {
    build: __dirname+'/public',
    src: __dirname+'/app/_source'
  };

  var files = fs.readdirSync('./grunt_tasks');

  files.forEach(function(file) {
    if (file === 'registers.js') return;
    var task = require('./grunt_tasks/'+file);
    grunt_cfg[task.name] = task.config;
  });

  grunt.initConfig(grunt_cfg);
  require('./grunt_tasks/registers.js')(grunt);
};
